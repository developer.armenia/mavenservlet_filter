package am.egs.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by User on 4/20/2019.
 */
public class ShowDataServlet extends HttpServlet {

    /**
     * Post Method.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        // Create a session object if it is already not  created.
        HttpSession session = request.getSession(true);
        session.setAttribute("UserName", request.getParameter("user"));

        PrintWriter out = response.getWriter();
        out.println("<html><body><h1>Post Method</h1>");
        out.println("<h1>user="+request.getParameter("user")+"</h1>");
        out.println("<h1>pass="+request.getParameter("pass")+"</h1>");
        out.println("<br><a href='secondServlet'>SecondPage</h1>");
        out.println("</body></html>");
    }
}
