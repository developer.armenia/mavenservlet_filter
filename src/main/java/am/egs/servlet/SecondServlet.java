package am.egs.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by User on 4/20/2019.
 */
public class SecondServlet extends HttpServlet {

    /**
     * Post Method.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        // Create a session object if it is already not  created.
        HttpSession session = request.getSession(true);

        PrintWriter out = response.getWriter();
        out.println("<html><body><h1>Session </h1>");
        out.println("<h1>user="+session.getAttribute("UserName")+"</h1>");
        out.println("<br><a href='showDataServlet'>showDataServlet</h1>");
        out.println("<br><a href='index.jsp'>index.jsp</h1>");
        out.println("</body></html>");
    }
}
